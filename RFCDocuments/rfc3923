





Network Working Group                                     P. Saint-Andre
Request for Comments: 3923                    Jabber Software Foundation
Category: Standards Track                                   October 2004


           End-to-End Signing and Object Encryption for the
           Extensible Messaging and Presence Protocol (XMPP)

Status of this Memo

   This document specifies an Internet standards track protocol for the
   Internet community, and requests discussion and suggestions for
   improvements.  Please refer to the current edition of the "Internet
   Official Protocol Standards" (STD 1) for the standardization state
   and status of this protocol.  Distribution of this memo is unlimited.

Copyright Notice

   Copyright (C) The Internet Society (2004).

Abstract

   This memo defines methods of end-to-end signing and object encryption
   for the Extensible Messaging and Presence Protocol (XMPP).

Table of Contents

   1.   Introduction . . . . . . . . . . . . . . . . . . . . . . . .   2
   2.   Requirements . . . . . . . . . . . . . . . . . . . . . . . .   2
   3.   Securing Messages  . . . . . . . . . . . . . . . . . . . . .   4
   4.   Securing Presence  . . . . . . . . . . . . . . . . . . . . .   9
   5.   Securing Arbitrary XMPP Data . . . . . . . . . . . . . . . .  13
   6.   Rules for S/MIME Generation and Handling . . . . . . . . . .  15
   7.   Recipient Error Handling . . . . . . . . . . . . . . . . . .  18
   8.   Secure Communications Through a Gateway  . . . . . . . . . .  20
   9.   urn:ietf:params:xml:xmpp-e2e Namespace . . . . . . . . . . .  21
   10.  application/xmpp+xml Media Type  . . . . . . . . . . . . . .  21
   11.  Security Considerations  . . . . . . . . . . . . . . . . . .  22
   12.  IANA Considerations  . . . . . . . . . . . . . . . . . . . .  22
   13.  References . . . . . . . . . . . . . . . . . . . . . . . . .  23
   A.   Schema for urn:ietf:params:xml:ns:xmpp-e2e . . . . . . . . .  26
   Author's Address. . . . . . . . . . . . . . . . . . . . . . . . .  26
   Full Copyright Statement. . . . . . . . . . . . . . . . . . . . .  27








Saint-Andre                 Standards Track                     [Page 1]

RFC 3923                        XMPP E2E                    October 2004


1.  Introduction

   This memo defines methods of end-to-end signing and object encryption
   for the Extensible Messaging and Presence Protocol (XMPP).  (For
   information about XMPP, see [XMPP-CORE] and [XMPP-IM].)  The method
   specified herein enables a sender to sign and/or encrypt an instant
   message sent to a specific recipient, sign and/or encrypt presence
   information that is directed to a specific user, and sign and/or
   encrypt any arbitrary XMPP stanza directed to a specific user.  This
   memo thereby helps the XMPP specifications meet the requirements
   specified in [IMP-REQS].

1.1.  Terminology

   This document inherits terminology defined in [CMS], [IMP-MODEL],
   [SMIME], and [XMPP-CORE].

   The capitalized key words "MUST", "MUST NOT", "REQUIRED", "SHALL",
   "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and
   "OPTIONAL" in this document are to be interpreted as described in BCP
   14, RFC 2119 [TERMS].

2.  Requirements

   For the purposes of this memo, we stipulate the following
   requirements:

   1.  The method defined MUST address signing and encryption
       requirements for minimal instant messaging and presence, as those
       are defined in [IMP-REQS].  In particular, the method MUST
       address the following requirements, which are copied here
       verbatim from [IMP-REQS]:

       *  The protocol MUST provide means to ensure confidence that a
          received message (NOTIFICATION or INSTANT MESSAGE) has not
          been corrupted or tampered with.  (Section 2.5.1)

       *  The protocol MUST provide means to ensure confidence that a
          received message (NOTIFICATION or INSTANT MESSAGE) has not
          been recorded and played back by an adversary.  (Section
          2.5.2)

       *  The protocol MUST provide means to ensure that a sent message
          (NOTIFICATION or INSTANT MESSAGE) is only readable by ENTITIES
          that the sender allows.  (Section 2.5.3)






Saint-Andre                 Standards Track                     [Page 2]

RFC 3923                        XMPP E2E                    October 2004


       *  The protocol MUST allow any client to use the means to ensure
          non-corruption, non-playback, and privacy, but the protocol
          MUST NOT require that all clients use these means at all
          times.  (Section 2.5.4)

       *  When A establishes a SUBSCRIPTION to B's PRESENCE INFORMATION,
          the protocol MUST provide A means of verifying the accurate
          receipt of the content B chooses to disclose to A.  (Section
          5.1.4)

       *  The protocol MUST provide A means of verifying that the
          presence information is accurate, as sent by B.  (Section
          5.3.1)

       *  The protocol MUST provide A means of ensuring that no other
          PRINCIPAL C can see the content of M.  (Section 5.4.6)

       *  The protocol MUST provide A means of ensuring that no other
          PRINCIPAL C can tamper with M, and B means to verify that no
          tampering has occurred.  (Section 5.4.7)

   2.  The method defined MUST enable interoperability with non-XMPP
       messaging systems that support the Common Presence and Instant
       Messaging (CPIM) specifications published by the Instant
       Messaging and Presence (IMPP) Working Group.  Two corollaries of
       this requirement are:

       *  Prior to signing and/or encrypting, the format of an instant
          message MUST conform to the CPIM Message Format defined in
          [MSGFMT].

       *  Prior to signing and/or encrypting, the format of presence
          information MUST conform to the CPP Presence Information Data
          Format defined in [PIDF].

   3.  The method MUST follow the required procedures (including the
       specific algorithms) defined in [CPIM] and [CPP].  In particular,
       these documents specify:

       *  Signing MUST use [SMIME] signatures with [CMS] SignedData.

       *  Encryption MUST use [SMIME] encryption with [CMS]
          EnvelopeData.

   4.  In order to enable interoperable implementations, sending and
       receiving applications MUST implement the algorithms specified
       under Mandatory-to-Implement Cryptographic Algorithms (Section
       6.10).



Saint-Andre                 Standards Track                     [Page 3]

RFC 3923                        XMPP E2E                    October 2004


   We further stipulate that the following functionality is out of scope
   for this memo:

   o  Discovery of support for this protocol.  An entity could discover
      whether another entity supports this protocol by (1) attempting to
      send signed or encrypted stanzas and receiving an error stanza
      ("technical" discovery) or a textual message in reply ("social"
      discovery) if the protocol is not supported, or (2) using a
      dedicated service discovery protocol, such as [DISCO] or [CAPS].
      However, the definition of a service discovery protocol is out of
      scope for this memo.

   o  Signing or encryption of XMPP groupchat messages, which are
      mentioned in [XMPP-IM] but not defined therein since they are not
      required by [IMP-REQS]; such messages are best specified in [MUC].

   o  Signing or encryption of broadcasted presence as described in
      [XMPP-IM] (the methods defined herein apply to directed presence
      only).

   o  Signing or encryption of communications that occur within the
      context of applications other than instant messaging and presence
      as those are described in [IMP-MODEL] and [IMP-REQS].

3.  Securing Messages

3.1.  Process for Securing Messages

   In order to sign and/or encrypt a message, a sending agent MUST use
   the following procedure:

   1.  Generate a "Message/CPIM" object as defined in [MSGFMT].

   2.  Sign and/or encrypt both the headers and content of the
       "Message/CPIM" object as specified in Requirement 3 of Section 2
       above.

   3.  Provide the resulting signed and/or encrypted object within an
       XML CDATA section (see Section 2.7 of [XML]) contained in an
       <e2e/> child of a <message/> stanza, where the <e2e/> element is
       qualified by the 'urn:ietf:params:xml:ns:xmpp-e2e' namespace as
       specified more fully in Section 9 below.

3.2.  Example of a Signed Message

   The following example illustrates the defined steps for signing a
   message.




Saint-Andre                 Standards Track                     [Page 4]

RFC 3923                        XMPP E2E                    October 2004


   First, the sending agent generates a "Message/CPIM" object in
   accordance with the rules and formats specified in [MSGFMT].

   Example 1: Sender generates "Message/CPIM" object:

   |   Content-type: Message/CPIM
   |
   |   From: Juliet Capulet <im:juliet@example.com>
   |   To: Romeo Montague <im:romeo@example.net>
   |   DateTime: 2003-12-09T11:45:36.66Z
   |   Subject: Imploring
   |
   |   Content-type: text/plain; charset=utf-8
   |   Content-ID: <1234567890@example.com>
   |
   |   Wherefore art thou, Romeo?

   Once the sending agent has generated the "Message/CPIM" object, the
   sending agent may sign it.  The result is a multipart [SMIME] object
   (see [MULTI]) that has a Content-Type of "multipart/signed" and
   includes two parts: one whose Content-Type is "Message/CPIM" and
   another whose Content-Type is "application/pkcs7-signature".





























Saint-Andre                 Standards Track                     [Page 5]

RFC 3923                        XMPP E2E                    October 2004


   Example 2: Sender generates multipart/signed object:

   |   Content-Type: multipart/signed; boundary=next;
   |                 micalg=sha1;
   |                 protocol=application/pkcs7-signature
   |
   |   --next
   |   Content-type: Message/CPIM
   |
   |   From: Juliet Capulet <im:juliet@example.com>
   |   To: Romeo Montague <im:romeo@example.net>
   |   DateTime: 2003-12-09T23:45:36.66Z
   |   Subject: Imploring
   |
   |   Content-type: text/plain; charset=utf-8
   |   Content-ID: <1234567890@example.com>
   |
   |   Wherefore art thou, Romeo?
   |   --next
   |   Content-Type: application/pkcs7-signature
   |   Content-Disposition: attachment;handling=required;\
   |                                   filename=smime.p7s
   |
   |   [signed body part]
   |
   |   --next--

   The sending agent now wraps the "multipart/signed" object in an XML
   CDATA section, which is contained in an <e2e/> element that is
   included as a child element of the XMPP message stanza and that is
   qualified by the 'urn:ietf:params:xml:ns:xmpp-e2e' namespace.




















Saint-Andre                 Standards Track                     [Page 6]

RFC 3923                        XMPP E2E                    October 2004


   Example 3: Sender generates XMPP message stanza:

   |   <message to='romeo@example.net/orchard' type='chat'>
   |     <e2e xmlns='urn:ietf:params:xml:ns:xmpp-e2e'>
   |   <![CDATA[
   |   Content-Type: multipart/signed; boundary=next;
   |                 micalg=sha1;
   |                 protocol=application/pkcs7-signature
   |
   |   --next
   |   Content-type: Message/CPIM
   |
   |   From: Juliet Capulet <im:juliet@example.com>
   |   To: Romeo Montague <im:romeo@example.net>
   |   DateTime: 2003-12-09T23:45:36.66Z
   |   Subject: Imploring
   |
   |   Content-type: text/plain; charset=utf-8
   |   Content-ID: <1234567890@example.com>
   |
   |   Wherefore art thou, Romeo?
   |   --next
   |   Content-Type: application/pkcs7-signature
   |   Content-Disposition: attachment;handling=required;\
   |                                   filename=smime.p7s
   |
   |   [signed body part]
   |
   |   --next--
   |   ]]>
   |     </e2e>
   |   </message>


3.3.  Example of an Encrypted Message

   The following example illustrates the defined steps for encrypting a
   message.

   First, the sending agent generates a "Message/CPIM" object in
   accordance with the rules and formats specified in [MSGFMT].










Saint-Andre                 Standards Track                     [Page 7]

RFC 3923                        XMPP E2E                    October 2004


   Example 4: Sender generates "Message/CPIM" object:

   |   Content-type: Message/CPIM
   |
   |   From: Juliet Capulet <im:juliet@example.com>
   |   To: Romeo Montague <im:romeo@example.net>
   |   DateTime: 2003-12-09T11:45:36.66Z
   |   Subject: Imploring
   |
   |   Content-type: text/plain; charset=utf-8
   |   Content-ID: <1234567890@example.com>
   |
   |   Wherefore art thou, Romeo?

   Once the sending agent has generated the "Message/CPIM" object, the
   sending agent may encrypt it.

   Example 5: Sender generates encrypted object:

   |   U2FsdGVkX19okeKTlLxa/1n1FE/upwn1D20GhPWqhDWlexKMUKYJInTWzERP+vcQ
   |   /OxFs40uc9Fx81a5/62p/yPb/UWnuG6SR6o3Ed2zwcusDImyyz125HFERdDUMBC9
   |   Pt6Z4cTGKBmJzZBGyuc3Y+TMBTxqFFUAxeWaoxnZrrl+LP72vwbriYc3KCMxDbQL
   |   Igc1Vzs5/5JecegMieNY24SlNyX9HMFRNFpbI64vLxYEk55A+3IYbZsluCFT31+a
   |   +GeAvJkvH64LRV4mPbUhENTQ2wbAwnOTvbLIaQEQrii78xNEh+MK8Bx7TBTvi4yH
   |   Ddzf9Sim6mtWsXaCAvWSyp0X91d7xRJ4JIgKfPzkxNsWJFCLthQS1p734eDxXVd3
   |   i08lEHzyll6htuEr59ZDAw==

   The sending agent now wraps the encrypted object in an XML CDATA
   section, which is contained in an <e2e/> element that is included as
   a child element of the XMPP message stanza and that is qualified by
   the 'urn:ietf:params:xml:ns:xmpp-e2e' namespace.

   Example 6: Sender generates XMPP message stanza:

   |   <message to='romeo@example.net/orchard' type='chat'>
   |     <e2e xmlns='urn:ietf:params:xml:ns:xmpp-e2e'>
   |   <![CDATA[
   |   U2FsdGVkX19okeKTlLxa/1n1FE/upwn1D20GhPWqhDWlexKMUKYJInTWzERP+vcQ
   |   /OxFs40uc9Fx81a5/62p/yPb/UWnuG6SR6o3Ed2zwcusDImyyz125HFERdDUMBC9
   |   Pt6Z4cTGKBmJzZBGyuc3Y+TMBTxqFFUAxeWaoxnZrrl+LP72vwbriYc3KCMxDbQL
   |   Igc1Vzs5/5JecegMieNY24SlNyX9HMFRNFpbI64vLxYEk55A+3IYbZsluCFT31+a
   |   +GeAvJkvH64LRV4mPbUhENTQ2wbAwnOTvbLIaQEQrii78xNEh+MK8Bx7TBTvi4yH
   |   Ddzf9Sim6mtWsXaCAvWSyp0X91d7xRJ4JIgKfPzkxNsWJFCLthQS1p734eDxXVd3
   |   i08lEHzy